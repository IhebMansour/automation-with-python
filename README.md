# Automation with Python

This project automates inventory management tasks using Python. The script reads data from a spreadsheet, performs calculations, identifies products with low inventory, and provides insights into inventory value per supplier.

## Getting Started

To get started with this project, follow these steps:

1. **Clone the Repository:**
   ```
   git clone https://gitlab.com/IhebMansour/automation-with-python.git
   ```

2. **Install Dependencies:**
   ```
   pip install openpyxl
   ```

3. **Prepare Your Data:**
   - Ensure your inventory data is stored in a spreadsheet named `inventory.xlsx`.
   - The spreadsheet should have the following columns:
     - Product ID
     - Inventory count
     - Price per unit
     - Supplier name

4. **Run the Script:**
   ```
   python inventory_management.py
   ```

5. **Check Output:**
   - The script will generate a new spreadsheet named `inventory_with_total_value.xlsx` containing updated inventory information.
   - Additionally, the script will print information about products with inventory less than 10, the number of products per supplier, and total inventory value per supplier.

## File Structure

- `inventory_management.py`: Python script for automating inventory management tasks.
- `inventory.xlsx`: Input spreadsheet containing inventory data.
- `inventory_with_total_value.xlsx`: Output spreadsheet containing updated inventory information.
- `README.md`: This file providing an overview of the project.

## Contributing

Contributions are welcome! If you'd like to contribute to this project, please follow these steps:

1. Fork the repository.
2. Create a new branch (`git checkout -b feature/improvement`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature/improvement`).
6. Create a new Merge Request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
